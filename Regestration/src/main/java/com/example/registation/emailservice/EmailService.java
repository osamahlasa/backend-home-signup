package com.example.registation.emailservice;

public interface

EmailService {
    void send(String to, String subject, String content);
}

