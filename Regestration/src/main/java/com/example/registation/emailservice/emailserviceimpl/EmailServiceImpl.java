package com.example.registation.emailservice.emailserviceimpl;

import com.example.registation.emailservice.EmailService;
import com.sendgrid.*;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Content;
import com.sendgrid.helpers.mail.objects.Email;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class EmailServiceImpl implements EmailService {

    @Value("${sendgrid.api.key}")
    private String sendgridApiKey;

    @Override
    public void send(String to, String subject, String content) {
        Email from = new Email("osamahlasa@gmail.com");
        Email toEmail = new Email(to);
        Content contentToSend = new Content("text/plain", content);
        Mail mail = new Mail(from, subject, toEmail, contentToSend);

        SendGrid sg = new SendGrid(sendgridApiKey);
        Request request = new Request();

        try {
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());

            sg.api(request);
        } catch (Exception ex) {
            System.out.println("Error sending email: " + ex.getMessage());
        }
    }
}
