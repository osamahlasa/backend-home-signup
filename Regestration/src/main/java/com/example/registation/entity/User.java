package com.example.registation.entity;

import jakarta.persistence.*;

@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_id")
    private int userId;

    @Column(name = "user_firstname", length = 255)
    private String firstName;

    @Column(name = "user_lastname", length = 255)
    private String lastName;

    @Column(name = "user_email", length = 255)
    private String email;

    @Column(name = "user_password", length = 255)
    private String password;

    @Column(name = "user_role", length = 255)
    private String role;

    @Column(name = "email_verification_token", length = 255)
    private String emailVerificationToken;  // New field for the email verification token

    @Column(name = "email_verified")
    private boolean emailVerified;  // New field for the email verification status

    public User() {
    }

    // Updated constructor
    public User(int userId, String firstName, String lastName, String email, String password, String role) {
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.role = role;
        this.emailVerificationToken = "";  // Default value for the email verification token
        this.emailVerified = false;  // Default value for the email verification status
    }


        public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getEmailVerificationToken() {
        return emailVerificationToken;
    }

    public void setEmailVerificationToken(String emailVerificationToken) {
        this.emailVerificationToken = emailVerificationToken;
    }

    public boolean isEmailVerified() {
        return emailVerified;
    }

    public void setEmailVerified(boolean emailVerified) {
        this.emailVerified = emailVerified;
    }



    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", role='" + role + '\'' +
                ", emailVerificationToken='" + emailVerificationToken + '\'' +
                ", emailVerified=" + emailVerified +
                '}';
    }



}
