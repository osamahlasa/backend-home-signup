package com.example.registation.repo;

import com.example.registation.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepo extends JpaRepository<User,Integer> {
    boolean existsByEmail(String userEmail);

    Optional<User> findByEmail(String email);

    Optional<User> findByEmailVerificationToken(String token);
}
