package com.example.registation.service;

import com.example.registation.dto.UserDTO;
import org.springframework.http.ResponseEntity;

import java.util.Map;

public interface UserService {
    ResponseEntity<Map<String, Object>> addUser(UserDTO userDTO);

    ResponseEntity<String> verifyUser(String token);
}
