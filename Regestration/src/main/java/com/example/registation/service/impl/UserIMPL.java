package com.example.registation.service.impl;

import com.example.registation.dto.UserDTO;
import com.example.registation.entity.User;
import com.example.registation.repo.UserRepo;
import com.example.registation.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import java.util.UUID;
import com.example.registation.emailservice.EmailService;



import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
public class UserIMPL implements UserService {

    private final Logger logger = LoggerFactory.getLogger(UserIMPL.class);

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private EmailService emailService; // Assuming you have a service to handle sending emails

    @Override
    public ResponseEntity<Map<String, Object>> addUser(UserDTO userDTO) {
        logger.info("Adding user with email: " + userDTO.getUserEmail());

        Optional<User> existingUser = userRepo.findByEmail(userDTO.getUserEmail());

        if (existingUser.isPresent()) {
            logger.info("User already exists with this email");

            Map<String, Object> response = new HashMap<>();
            response.put("status", "failed");
            response.put("message", "Email already exists");

            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }

        User user = new User();
        user.setUserId(userDTO.getUserId());
        user.setFirstName(userDTO.getUserFirstName());
        user.setLastName(userDTO.getUserLastName());
        user.setEmail(userDTO.getUserEmail());
        user.setPassword(passwordEncoder.encode(userDTO.getUserPassword()));
        user.setRole(userDTO.getUserRole());
        user.setEmailVerificationToken(UUID.randomUUID().toString()); // email_verification_token
        user.setEmailVerified(false); // email_verified

        userRepo.save(user);
        // Send verification email
        String emailContent = "Please click the following link to verify your email: http://10.45.2.25:8085/api/v1/user/verify?token=" + user.getEmailVerificationToken();
        emailService.send(user.getEmail(), "Verify your email", emailContent);


        Map<String, Object> response = new HashMap<>();
        response.put("status", "success");http://10.45.2.25:8085/api/v1/user/verify?token="
        response.put("message", "User created successfully.");

        logger.info("User created successfully with email: " + userDTO.getUserEmail());

        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<String> verifyUser(String token) {
        Optional<User> optionalUser = userRepo.findByEmailVerificationToken(token);
        if (!optionalUser.isPresent()) {
            return new ResponseEntity<>("Invalid token", HttpStatus.BAD_REQUEST);
        }

        User user = optionalUser.get();
        user.setEmailVerified(true);
        userRepo.save(user);

        String htmlResponse = "<html>" +
                "<body style='background-color: #f0f0f0; font-family: Arial, sans-serif;'>" +
                "<div style='max-width: 600px; margin: auto; padding: 20px; background-color: #fff; border-radius: 4px; box-shadow: 0 0 10px rgba(0, 0, 0, 0.05);'>" +
                "<h1 style='color: #446dff; text-align: center;'>Email Verified Successfully</h1>" +
                "<p style='color: #333; font-size: 18px; text-align: center;'>Your email has been verified successfully. You can now return to the application and log in.</p>" +
                "</div>" +
                "</body>" +
                "</html>";

        return ResponseEntity.ok().contentType(MediaType.TEXT_HTML).body(htmlResponse);
    }

}
